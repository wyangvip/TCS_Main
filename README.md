# TransCodeServer ![badge-mit](https://img.shields.io/badge/LICENSE-AGPL3-brightgreen.svg) ![badge-php](https://img.shields.io/badge/Go-lang-brightgreen.svg)

## 开发进度

初版开发中

## 运行环境

支持系统:Windows Linux

注:Linux需要另外安装FFmpeg

## 感谢
我们使用了以下开源项目 郑重感谢

[FFmpeg](https://github.com/FFmpeg/FFmpeg)

[MDUI](https://github.com/zdhxiong/mdui)

部分Go内引用的过于繁杂 并未列出

## 加入我们

此项目QQ群:674511398 | PackTeam讨论群:296114195 