package main

import (
	"fmt"
	"github.com/valyala/fasthttp"
	"io"
	"io/ioutil"
	"os"
	"path"
	"strconv"
)

func Resource(w *fasthttp.RequestCtx)  {
	OpenFile, err := os.Open("template"+string(w.URI().Path()))
	if err != nil {
		w.SetStatusCode(404)
		fmt.Println(string(w.URI().Path()))
		return
	}
	//获取文件类型
	FileStat, _ := OpenFile.Stat()
	FileExt := path.Ext(string(w.URI().Path()))
	var FileContentType string
	switch FileExt {
	case ".js":
		FileContentType = "application/javascript"
	case ".css":
		FileContentType = "text/css"
	case ".woff":
		FileContentType = "application/font-woff"
	case ".woff2":
		FileContentType = "font/woff2"
	}
	FileSize := strconv.FormatInt(FileStat.Size(), 10)
	//写入HTTP头
	w.Response.Header.Set("Content-Type", FileContentType)
	w.Response.Header.Set("Content-Length", FileSize)
	//重置文件指针
	_, err = OpenFile.Seek(0, 0)
	//发送文件
	w.Response.Header.Set("Server", "PackEngine-WEB/Core v1.0")
	_, err = io.Copy(w, OpenFile)
	return
}

func Index(w *fasthttp.RequestCtx)  {
	//登录状态判断
	if !CheckLoginStatus(w){
		w.Redirect("login",302)
		return
	}
	OpenFile, err := os.Open("template/index.html")
	File,err := ioutil.ReadAll(OpenFile)
	if err != nil {
		_ = OpenFile.Close()
		w.SetStatusCode(404)
		fmt.Println(string(w.URI().Path()))
		return
	}
	FileStat, _ := OpenFile.Stat()
	FileSize := strconv.FormatInt(FileStat.Size(), 10)
	_ = OpenFile.Close()
	//写入HTTP头
	w.Response.Header.Set("Content-Type", "text/html")
	w.Response.Header.Set("Content-Length", FileSize)
	w.Response.Header.Set("Cache-Control", "no-cache")
	//发送文件
	w.Response.Header.Set("Server", "PackEngine-WEB/Core v1.0")
	_,_ = w.Write(File)
	return
}

func Dashboard(w *fasthttp.RequestCtx)  {
	//登录状态判断
	if !CheckLoginStatus(w){
		//写入HTTP头
		w.Response.Header.Set("Content-Type", "text/html")
		w.Response.Header.Set("Cache-Control", "no-cache")
		//发送文件
		w.Response.Header.Set("Server", "PackEngine-WEB/Core v1.0")
		_,_ = w.WriteString("<script>window.parent.location.href='login'</script>")
		return
	}
	OpenFile, err := os.Open("template/dashboard.html")
	File,err := ioutil.ReadAll(OpenFile)
	if err != nil {
		_ = OpenFile.Close()
		w.SetStatusCode(404)
		fmt.Println(string(w.URI().Path()))
		return
	}
	FileStat, _ := OpenFile.Stat()
	FileSize := strconv.FormatInt(FileStat.Size(), 10)
	_ = OpenFile.Close()
	//写入HTTP头
	w.Response.Header.Set("Content-Type", "text/html")
	w.Response.Header.Set("Content-Length", FileSize)
	w.Response.Header.Set("Cache-Control", "no-cache")
	//发送文件
	w.Response.Header.Set("Server", "PackEngine-WEB/Core v1.0")
	_,_ = w.Write(File)
	return
}

func Login(w *fasthttp.RequestCtx) {
	if CheckLoginStatus(w){
		w.Redirect("index",302)
		return
	}
	OpenFile, err := os.Open("template/login.html")
	File,err := ioutil.ReadAll(OpenFile)
	if err != nil {
		_ = OpenFile.Close()
		w.SetStatusCode(404)
		fmt.Println(string(w.URI().Path()))
		return
	}
	FileStat, _ := OpenFile.Stat()
	FileSize := strconv.FormatInt(FileStat.Size(), 10)
	_ = OpenFile.Close()
	//写入HTTP头
	w.Response.Header.Set("Content-Type", "text/html")
	w.Response.Header.Set("Content-Length", FileSize)
	w.Response.Header.Set("Cache-Control", "no-cache")
	//发送文件
	w.Response.Header.Set("Server", "PackEngine-WEB/Core v1.0")
	_,_ = w.Write(File)
	return
}