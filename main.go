package main

import (
	"bytes"
	"crypto/md5"
	"database/sql"
	"encoding/hex"
	"fmt"
	"github.com/Unknwon/goconfig"
	_ "github.com/mattn/go-sqlite3"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"
)

var (
	ServerVersion = "1.0.00"
	ReleaseType   = "Beta"
	Database      *sql.DB
	err           error
	LogFile       *os.File
	//Config
	ConfigFile         *goconfig.ConfigFile
	EncodeThread       int
	EncodeBitrateVideo int
	EncodeBitrateAudio int
	EncodeSliceTime    int
	EncodeSliceFrame   int
	EncodeTwoPass      bool
	EncodeHLS          bool
	EncodeDASH         bool
	EncodeFramerate    int
	RealTime           bool
	RealTimeCache      int
	RealTimePreload    int
	EncodeRes          string
	//MultiThread
	ThreadPool    [100]int
	BufferPool    [100]bytes.Buffer
	ThreadProcess [100]float64
	WaitCount     int64
	//Signal
	ReloadSignal      bool
	FileSignal        bool
	FileRunningSignal bool
	ExitSignal        chan os.Signal
)

func init() {
	LogFile, err = os.OpenFile("main.log", os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	MultiWriter := io.MultiWriter(os.Stdout, LogFile)
	log.SetOutput(MultiWriter)
	ExitSignal = make(chan os.Signal)
	signal.Notify(ExitSignal, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	go func() {
		for s := range ExitSignal {
			switch s {
			case syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM:
				log.Println("正在执行退出清理")
				log.Print("\n\n\n\n")
				ExitFunc()
			default:
				fmt.Println("其他信号:", s)
			}
		}
	}()
	log.Println("[Init] TransCodeServer ", ServerVersion)
	if ReleaseType == "Beta" {
		log.Println("[WARN] 你正在运行测试版本的TCS")
	}
	log.Println("[Init] 开始加载数据库")
	ConfigFile, err = goconfig.LoadConfigFile("config.cfg")
	if err != nil {
		log.Panicln("[PANIC] 无法正常读取配置文件")
	}
	Database, err = sql.Open("sqlite3", "./Data.db")
	if err != nil {
		log.Panicln("[PANIC] 无法正常读取数据库")
	}
	LoadConfig()
	ShowHardware()
	LoadControlPanel()
}

func main() {
	defer func() {
		log.Println("正在执行崩溃清理")
		_ = Database.Close()
	}()
	LogOutCount := 0
	for {
		if ReloadSignal {
			log.Println("[Signal]已检测到重载信号 正在重载配置")
			LoadConfig()
			ReloadSignal = false
		}
		if FileSignal {
			if !FileRunningSignal {
				log.Println("[Signal]检测到扫描信号 开始扫描转码文件")
				FileRunningSignal = true
				go FileWorker()
			}
		}
		//获取等待队列
		err = Database.QueryRow("SELECT count(*) FROM VideoList WHERE Status = 0").Scan(&WaitCount)
		if WaitCount == 0 {
			if LogOutCount == 0 {
				log.Println("[Thread] 队列空闲")
				LogOutCount = 1
			}
		} else {
			LogOutCount = 0
			log.Println("[Thread] 等待队列:", WaitCount)
			//进程调度
			for Now := 1; Now <= EncodeThread; Now++ {
				if ThreadPool[Now] != 0 {
					log.Println("[Thread]", Now, ":工作中")
				} else {
					log.Println("[Thread]", Now, ":空闲")
					if WaitCount > 0 {
						var VideoID int
						err = Database.QueryRow("SELECT ID FROM VideoList WHERE Status = 0 LIMIT 1").Scan(&VideoID)
						if err == nil {
							log.Println("[Thread]", Now, "已经分配任务:", VideoID)
							ThreadPool[Now] = 1
							go Worker(Now, VideoID)
						}
					}
				}
			}
		}
		time.Sleep(5 * time.Second)
	}
}

func FileWorker() {
	DirData, err := ioutil.ReadDir("upload")
	if err != nil {
		log.Println("[File] 获取文件夹信息错误")
		return
	}
	NewInsert, err := Database.Prepare("INSERT INTO `VideoList` (ID, Name, Random, TimeDay, Time, Status, MD5) VALUES (NULL,?,?,?,?,?,?)")
	if err != nil {
		fmt.Println("[File] 数据库错误", err.Error())
		return
	}
	for _, Filename := range DirData {
		MD5File, err := os.Open("upload/" + Filename.Name())
		if err != nil {
			fmt.Println("[File] 无法读取文件", Filename.Name())
			return
		}
		MD5Hash := md5.New()
		if _, err := io.Copy(MD5Hash, MD5File); err != nil {
			fmt.Println("[File] 无法计算文件MD5 ", Filename.Name())
			return
		}
		FileMD5 := hex.EncodeToString(MD5Hash.Sum(nil))
		_ = MD5File.Close()
		var SameID int
		var TimeDay int64
		NowTime := time.Now().Format("2006-01-02")
		t, _ := time.Parse("2006-01-02", NowTime)
		t = t.Add(-8 * time.Hour)
		TimeDay = t.Unix()
		err = Database.QueryRow("SELECT ID FROM VideoList WHERE MD5 = '" + FileMD5 + "'").Scan(&SameID)
		if err == sql.ErrNoRows {
			//
			VideoRandom := RandStringRunes(8)
			_, err = NewInsert.Exec(Filename.Name(), VideoRandom, TimeDay, time.Now().Unix(), 0, FileMD5)
			if err != nil {
				fmt.Println("[File] 插入数据库", err.Error())
				return
			}
			//Move File
			FileExt := filepath.Ext(Filename.Name())
			_ = os.Rename("upload/"+Filename.Name(), "encoding/"+VideoRandom+FileExt)
			log.Println("[File] 已分配新的待转码文件", VideoRandom+FileExt)
		} else {
			log.Println("[File] 文件已存在", Filename.Name())
		}
	}
	FileSignal = false
	FileRunningSignal = false
}

func LoadConfig() {
	fmt.Println("============开始加载配置============")
	//Read Encode Thread
	var EncodeThreadRow string
	err = Database.QueryRow("SELECT Data FROM Config WHERE Name = 'EncodeThread'").Scan(&EncodeThreadRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 转码线程数")
	}
	EncodeThread, err = strconv.Atoi(EncodeThreadRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 转码线程数")
	}
	log.Println("[Config] 转码线程数", EncodeThread)
	//Read Encode Video Bitrate
	var EncodeBitrateVideoRow string
	err = Database.QueryRow("SELECT Data FROM Config WHERE Name = 'EncodeBitrateVideo'").Scan(&EncodeBitrateVideoRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 视频比特率")
	}
	EncodeBitrateVideo, err = strconv.Atoi(EncodeBitrateVideoRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 视频比特率")
	}
	log.Println("[Config] 视频比特率", EncodeBitrateVideo, "Kbps")
	//Read Encode Audio Bitrate
	var EncodeBitrateAudioRow string
	err = Database.QueryRow("SELECT Data FROM Config WHERE Name = 'EncodeBitrateAudio'").Scan(&EncodeBitrateAudioRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 音频比特率")
	}
	EncodeBitrateAudio, err = strconv.Atoi(EncodeBitrateAudioRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 音频比特率")
	}
	log.Println("[Config] 音频比特率", EncodeBitrateAudio, "Kbps")
	//Read Encode Slice Time
	var EncodeSliceTimeRow string
	err = Database.QueryRow("SELECT Data FROM Config WHERE Name = 'EncodeSliceTime'").Scan(&EncodeSliceTimeRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 分片时间")
	}
	EncodeSliceTime, err = strconv.Atoi(EncodeSliceTimeRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 分片时间")
	}
	log.Println("[Config] 分片时间", EncodeSliceTime, "秒")
	//Read Encode Slice Frame
	var EncodeSliceFrameRow string
	err = Database.QueryRow("SELECT Data FROM Config WHERE Name = 'EncodeSliceFrame'").Scan(&EncodeSliceFrameRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 分片帧数")
	}
	EncodeSliceFrame, err = strconv.Atoi(EncodeSliceFrameRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 分片帧数")
	}
	log.Println("[Config] 分片帧数", EncodeSliceFrame, "帧")
	//Read Encode Framerate
	var EncodeFramerateRow string
	err = Database.QueryRow("SELECT Data FROM Config WHERE Name = 'EncodeFramerate'").Scan(&EncodeFramerateRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 视频帧数")
	}
	EncodeFramerate, err = strconv.Atoi(EncodeFramerateRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 视频帧数")
	}
	log.Println("[Config] 视频帧数", EncodeFramerate, "帧")
	//Read Encode Res
	err = Database.QueryRow("SELECT Data FROM Config WHERE Name = 'EncodeRes'").Scan(&EncodeRes)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 分辨率")
	}
	log.Println("[Config] 分辨率", EncodeRes)
	//Read Encode 2 Pass
	var EncodeTwoPassRow string
	err = Database.QueryRow("SELECT Data FROM Config WHERE Name = 'EncodeTwoPass'").Scan(&EncodeTwoPassRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 2Pass编码")
	}
	if EncodeTwoPassRow == "0" {
		EncodeTwoPass = false
		log.Println("[Config] 2Pass 编码未启用")
	} else {
		EncodeTwoPass = true
		log.Println("[Config] 2Pass 编码已启用")
	}
	//Read HLS
	var EncodeHLSRow string
	err = Database.QueryRow("SELECT Data FROM Config WHERE Name = 'EncodeHLS'").Scan(&EncodeHLSRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 HLS设置")
	}
	if EncodeHLSRow == "0" {
		EncodeHLS = false
		log.Println("[Config] HLS 编码未启用")
	} else {
		EncodeHLS = true
		log.Println("[Config] HLS 编码已启用")
	}
	//Read DASH
	var EncodeDASHRow string
	err = Database.QueryRow("SELECT Data FROM Config WHERE Name = 'EncodeDASH'").Scan(&EncodeDASHRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 DASH设置")
	}
	if EncodeDASHRow == "0" {
		EncodeDASH = false
		log.Println("[Config] DASH 编码未启用")
	} else {
		EncodeDASH = true
		log.Println("[Config] DASH 编码已启用")
	}
	//Read Real Time Encode
	var RealTimeRow string
	err = Database.QueryRow("SELECT Data FROM Config WHERE Name = 'RealTime'").Scan(&RealTimeRow)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 虚拟切片设置")
	}
	if RealTimeRow == "0" {
		RealTime = false
		log.Println("[Config] 虚拟切片未启用")
	} else {
		RealTime = true
		log.Println("[Config] 虚拟切片已启用")
		log.Println("[WARN] 仅推荐在前端有CDN缓存的情况下使用虚拟切片 如访问量过大可能导致CPU负载过大")
	}
	//Read Real Time Cache
	err = Database.QueryRow("SELECT Data FROM Config WHERE Name = 'RealTimeCache'").Scan(&RealTimeCache)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 虚拟切片缓存大小")
	}
	if RealTimeCache == 0 {
		log.Println("[Config] 虚拟切片缓存未启用")
	} else {
		log.Println("[Config] 虚拟切片缓存:", RealTimeCache, "MB")
	}
	//Read Real Time Preload
	err = Database.QueryRow("SELECT Data FROM Config WHERE Name = 'RealTimeCache'").Scan(&RealTimePreload)
	if err != nil {
		log.Panicln("[PANIC] 无法读取 虚拟切片预加载")
	}
	if RealTimePreload == 0 {
		log.Println("[Config] 虚拟切片预加载未启用")
	} else {
		log.Println("[Config] 虚拟切片预加载:", RealTimePreload, "分片")
	}
}

func ShowHardware() {
	fmt.Println("==============配置信息==============")
	log.Println("CPU数量:", runtime.NumCPU())
	if runtime.GOOS == "windows" {
		GetGPUWindows()
	} else {
		log.Println("非Windows平台暂无法显示GPU")
	}
}

func GetGPUWindows() {
	Info := exec.Command("cmd", "/C", "wmic path win32_VideoController get name")
	Info.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
	History, _ := Info.Output()
	Result := strings.Replace(string(History), "Name", "", -1)
	Return := strings.Split(Result, "\r\n")
	GPUNumber := len(Return) - 3
	log.Println("GPU数量:", GPUNumber)
	if GPUNumber > 1 {
		log.Println("检测到多个GPU 将自动将任务均分至各个GPU")
	}
	if len(Return) > 2 {
		for i := 1; i < len(Return)-2; i++ {
			log.Println("[", i-1, "]", Return[i])
		}
	}
}

func RandStringRunes(n int) string {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func ExitFunc() {
	_ = Database.Close()
	os.Exit(0)
}

func identifyPanic() string {
	var name, file string
	var line int
	var pc [16]uintptr

	n := runtime.Callers(3, pc[:])
	for _, pc := range pc[:n] {
		fn := runtime.FuncForPC(pc)
		if fn == nil {
			continue
		}
		file, line = fn.FileLine(pc)
		name = fn.Name()
		if !strings.HasPrefix(name, "runtime.") {
			break
		}
	}

	switch {
	case name != "":
		return fmt.Sprintf("%v:%v", name, line)
	case file != "":
		return fmt.Sprintf("%v:%v", file, line)
	}

	return fmt.Sprintf("pc:%x", pc)
}
