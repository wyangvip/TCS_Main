package main

import "C"
import (
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
	"time"
)

type AjaxReturnDefault struct {
	Code    int    `json:"code"`
	Message string `json:"msg"`
}

func AjaxLogin(w *fasthttp.RequestCtx) {
	PostValues := w.PostArgs()
	if err != nil {
		Return := AjaxReturnDefault{
			101,
			"用户名和密码不得为空",
		}
		ReturnEcho, err := json.Marshal(Return)
		if err != nil {
			fmt.Println(err)
		}
		_, _ = w.Write(ReturnEcho)
		return
	}
	Username := PostValues.Peek("username")
	Password := PostValues.Peek("password")
	//写入HTTP头
	w.Response.Header.Set("Content-Type", "text/html")
	w.Response.Header.Set("Cache-Control", "no-cache")
	w.Response.Header.Set("Server", "PackEngine-WEB/Core v1.0")
	if Username == nil || Password == nil {
		Return := AjaxReturnDefault{
			101,
			"用户名和密码不得为空",
		}
		ReturnEcho, err := json.Marshal(Return)
		if err != nil {
			fmt.Println(err)
		}
		_, _ = w.Write(ReturnEcho)
		return
	}
	RealUsername, _ := ConfigFile.GetValue("Control", "Username")
	RealPassword, _ := ConfigFile.GetValue("Control", "Password")
	if string(Username) == RealUsername && string(Password) == RealPassword {
		//生成登录验证
		LoginRand := RandStringRunes(64)
		//写入Cookies
		LoginCookies := fasthttp.Cookie{}
		LoginCookies.SetKey("EngineLogin")
		LoginCookies.SetValue(LoginRand)
		LoginCookies.SetPath("/")
		w.Response.Header.SetCookie(&LoginCookies)
		//写入缓存
		Session.Set(LoginRand, []byte("1"), 1*time.Hour)
		Return := AjaxReturnDefault{
			201,
			"登录成功 跳转中",
		}
		ReturnEcho, err := json.Marshal(Return)
		if err != nil {
			fmt.Println(err)
		}
		_, _ = w.Write(ReturnEcho)
		return
	} else {
		Return := AjaxReturnDefault{
			101,
			"用户名或密码错误",
		}
		ReturnEcho, err := json.Marshal(Return)
		if err != nil {
			fmt.Println(err)
		}
		_, _ = w.Write(ReturnEcho)
		return
	}
}

func AjaxScanStatus(w *fasthttp.RequestCtx) {
	type AjaxReturn struct {
		Code    int    `json:"code"`
		Status  int    `json:"status"`
	}
	//登录状态判断
	if !CheckLoginStatus(w) {
		Return := AjaxReturnDefault{
			101,
			"登录状态已失效",
		}
		ReturnEcho, err := json.Marshal(Return)
		if err != nil {
			fmt.Println(err)
		}
		_, _ = w.Write(ReturnEcho)
		return
	}
	//判断当前File是否运行中
	if FileSignal {
		Return := AjaxReturn{
			201,
			1,
		}
		ReturnEcho, err := json.Marshal(Return)
		if err != nil {
			fmt.Println(err)
		}
		_, _ = w.Write(ReturnEcho)
		return
	}else{
		Return := AjaxReturn{
			201,
			0,
		}
		ReturnEcho, err := json.Marshal(Return)
		if err != nil {
			fmt.Println(err)
		}
		_, _ = w.Write(ReturnEcho)
		return
	}
}

func AjaxStartScan(w *fasthttp.RequestCtx)  {
	if !CheckLoginStatus(w) {
		Return := AjaxReturnDefault{
			101,
			"登录状态已失效",
		}
		ReturnEcho, err := json.Marshal(Return)
		if err != nil {
			fmt.Println(err)
		}
		_, _ = w.Write(ReturnEcho)
		return
	}
	FileSignal=true
	Return := AjaxReturnDefault{
		201,
		"已经开始扫描",
	}
	ReturnEcho, err := json.Marshal(Return)
	if err != nil {
		fmt.Println(err)
	}
	_, _ = w.Write(ReturnEcho)
	return
}
